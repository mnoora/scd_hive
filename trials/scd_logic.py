# Databricks notebook source
###############################################################
# Step 0: Imports and Spark session creation
###############################################################
from pyspark.sql import SparkSession
from pyspark.sql.functions import map_values,map_concat,col,map_keys
spark = SparkSession.builder.master("yarn").appName("PySpark CAR Poc").getOrCreate()

# COMMAND ----------

# validate filesystem of source location
display(dbutils.fs.ls("/FileStore/datasets/citi/scd_transactions"))
#display(dbutils.fs.ls("/FileStore/datasets/citi/scd_transactions/category=CAT_0"))

# COMMAND ----------

###########################################################################################################
# Step 1: Read the source transaction table to pyspark dataframe
###########################################################################################################
# compute stats
spark.sql("ANALYZE TABLE scd_transactions COMPUTE STATISTICS")

# Reading data source from parquet table
transactions = spark.sql("select * from scd_transactions")
print("Dataframe shape is {}x{}".format(transactions.count(),len(transactions.columns)))
transactions.printSchema()

# COMMAND ----------

#transactions.show(truncate=False)
display(transactions)

# COMMAND ----------

def search_object(database, table):
	if len([(i) for i in spark.catalog.listTables(database) if i.name==str(table)]) != 0:
		return True
	return False	

mydatabase = 'default'
mytable = 'scd_transactions' 
check_table = search_object(mydatabase,mytable)
check_table

# COMMAND ----------

# distinct category
cats = transactions.select("category").rdd.flatMap(lambda x: x).distinct().collect()
print(cats)

# COMMAND ----------

if (pyspark.sql.HiveContext("SHOW TABLES LIKE '" + mytable + "'").count() == 1) {
    println(tableName + " exists")
}

count = spark.sql("("SHOW TABLES LIKE '" + mytable + "'").count()")

# COMMAND ----------

# First run
max_load_offset = 0

updated = transactions

cats = transactions.select("category").distinct().collect()
  #print(cats)

for cat in cats:
  #print(type(cat))
  df_cat = None
  cat_type = cat['category']
  cat_table_name = "feature_store_{}".format(cat_type)
  filter_condition = "category = '{}'".format(cat_type)
  #print(filter_condition)
  #print(cat_table_name)    
  df_cat = res2.filter(filter_condition)    
  with T(cat_type + ":Task D[B1]: Used map_keys function to Extract keys"):
    features = (df_cat \
          .select(map_keys("featuremap")) \
          .distinct() \
          .rdd.flatMap(lambda x: x[0]).collect())  
  with T(cat_type + ":Task D[B2]: Used explode method to Extract map keys"):
    features = (df_cat \
          .select(explode("featuremap")) \
          .select("key") \
          .distinct() \
          .rdd.flatMap(lambda x: x).collect())    
  with T(cat_type + ":Task D[C]: Generate new cols from map to dataframe"):
    exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")] + [col("featuremap").getItem(k).alias(k) for k in features]
    final_df_cat = df_cat.select(* exprs)
  with T(cat_type + ":Task D[D]: Aggregating values in dataframe"): 
    # Defines partitioning specification and ordering specification.
    windowSpec = \
        Window \
            .partitionBy("cust_id") \
            .orderBy(asc("load_offset"))
    # Defines a Window Specification with a ROW frame.
    windowSpec.rowsBetween(Window.unboundedPreceding, Window.currentRow)
    # Defines a Window Specification with a RANGE frame.
    #windowSpec.rangeBetween(start, end)
    transformations = [(last(feature,ignorenulls=True)).over(windowSpec).alias(feature) for feature in features]
    final_df = final_df_cat.select("cust_id", "rec_eff_date","load_offset", *transformations)      
  with T(cat_type + ":Task D[E]: Writing final dataframe results to table " + cat_table_name):
    final_df.coalesce(16).write.mode('overwrite').format("parquet") \
              .sortBy("load_offset") \
              .partitionBy("rec_eff_date") \
              .bucketBy(10,"cust_id") \
              .option("path", outputpath + cat_table_name) \
              .saveAsTable(cat_table_name)

