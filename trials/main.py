# import modules
import json
import importlib
import argparse

from dependencies import logger
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession

def parse_arguments():
    """ Parse arguments provided by spark-submit command"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--job", required=True)
    parser.add_argument("--rec_eff_dt", required=True)
    return parser.parse_args()

def main():
    """ Main function excecuted by spark-submit command"""
    args = parse_arguments()
    job_name=args.job
    
    with open("config.json", "r") as config_file:
        config = json.load(config_file)

    # spark = SparkSession.builder.master('local').getOrCreate()
    spark = SparkSession.builder.appName(config.get("app_name")).getOrCreate()
    
    spark.sparkContext.setLogLevel("ERROR")
    logger.info("Starting spark application")

    job_module = importlib.import_module(f"jobs.{job_name.job}")
    config["rec_eff_dt"]=args.rec_eff_dt
    job_module.run_job(spark, config)
    
    spark.stop()
    return None

if __name__ == "__main__":
    main()
    sys.exit()