# Databricks notebook source
###############################################################
# Step 0: Imports and Spark session creation
###############################################################
from pyspark.sql import SparkSession
from pyspark.sql.functions import map_values,map_concat,col,map_keys
spark = SparkSession.builder.master("yarn").appName("PySpark CAR Poc").getOrCreate()

# COMMAND ----------

###########################################################################################################
# Step 1A: Generating Sample Data with 3 maptype columns, creating spark Dataframe, writing to Parquet file
###########################################################################################################
import numpy as np
import pandas as pd
import string
from datetime import datetime

# Create a pyspark dataframe with a sample data using maps
from pyspark.sql.types import StructField, StructType, StringType, IntegerType,FloatType, BooleanType, MapType, DateType, TimestampType, ArrayType

# list of pyspark.sql.types for reference
#- "DataType", "NullType", "StringType", "BinaryType", "BooleanType", "DateType",
#    "TimestampType", "DecimalType", "DoubleType", "FloatType", "ByteType", "IntegerType",
#    "LongType", "ShortType", "ArrayType", "MapType", "StructField", "StructType"
from pyspark.sql.functions import map_from_arrays, create_map, array, map_from_entries

schema = StructType([
  StructField('cust_id', IntegerType(), True), 
  StructField('category', StringType(), True),
  StructField('rec_eff_dt', DateType(), True),
  StructField('load_offset', IntegerType(), True),
  StructField('feature_map1', MapType(StringType(), IntegerType()), True),
  StructField('feature_map2', MapType(StringType(), FloatType()), True),
  StructField('feature_map3', MapType(StringType(), BooleanType()), True)
])

#50 million
#incremental rows of 1 million, 5, 10, 20, 30, 40, 50
# cols upto 2000 [ 10, 50, 100, 200, 500, 1000 and 2000]
# categories upto 

# Data sampling Variables
n_categories = 5
n_rows_per_category =10
n_rows = (n_rows_per_category*n_categories)
limit_batch_size_for_writes = 100
n_cols = 12
n_maps = 3

n_features = 200
n_customers = n_rows
n_offsets = 4
max_map_size = (n_cols/n_maps)
min_map_size = max_map_size/2

dataDictionary = []
#partition by category, effec_date and load_offsets

customers = [ i for i in range(n_customers)]
categories = ['CAT_%d' % i for i in range(n_categories)]
effective_dates = ['2021-01-01']
features=['F%d' % i for i in range(n_features)]
boolean_values = [False, True]
string_values = list(string.ascii_lowercase)

output_path="/FileStore/datasets/citi/"
table_name = "scd_transactions"

if limit_batch_size_for_writes > n_rows:
  limit_batch_size_for_writes = 100
else:
  limit_batch_size_for_writes = n_rows

writes=0  
counter=1
for i in range(n_rows):
    # Generate vector of random customer ids
    Customers_Id = customers[i]
    #print(type(Customers_Id))
    Category = np.random.choice(categories, 1).tolist()[0]
    #print(type(Category))
    Effective_Date = np.random.choice(effective_dates, 1).tolist()[0]
    Effective_Date = datetime.strptime(Effective_Date,'%Y-%m-%d')
    #print(type(Effective_Date))
    # 22 days(24*24*60) of minutes possible values from rec_eff_date midnight
    Load_Offset = np.random.randint(1, n_offsets, 1)[0].item()
    #print(type(Load_Offset))
    # generating random map sizes for each map type
    random_map1_size = np.random.randint(min_map_size,max_map_size,1)[0]
    random_map2_size = np.random.randint(min_map_size,max_map_size,1)[0]
    random_map3_size = np.random.randint(min_map_size,max_map_size,1)[0]
    # generating feature keys for each data type
    fkeys_map1 = np.random.choice(features, random_map1_size).tolist()
    fkeys_map2 = np.random.choice(features, random_map2_size).tolist()
    fkeys_map3 = np.random.choice(features, random_map3_size).tolist()
    #print(type(fkeys_map1[0]))
    # generating feature values 
    fvalues_map1 = np.random.randint(1, 100, random_map1_size).tolist()
    fvalues_map2 = np.random.random_sample(random_map2_size, ).tolist()
    fvalues_map3 = np.random.choice(boolean_values, random_map3_size).tolist()
    #print(type(fvalues_map1[0]))    
    dict_map1 = {str(fkeys_map1[i]): fvalues_map1[i] for i in range(len(fkeys_map1))}
    dict_map2 = {str(fkeys_map2[i]): fvalues_map2[i] for i in range(len(fkeys_map2))}
    dict_map3 = {str(fkeys_map3[i]): fvalues_map3[i] for i in range(len(fkeys_map3))}
    dataObj = (Customers_Id,Category,Effective_Date,Load_Offset,dict_map1,dict_map2,dict_map3)
    #print(dataObj)
    dataDictionary.append(dataObj)
    df = spark.createDataFrame(data=dataDictionary, schema = schema)
    df.write.mode('overwrite').format("parquet") \
          .partitionBy("category","rec_eff_date","load_offset") \
          .option("path", output_path + table_name) \
          .saveAsTable(table_name)
    #print("{}:{}".format(i,counter))

print("total rows:{}; last iter counter: {}; total number of writes {}".format(i,counter,writes))
#df = spark.createDataFrame(data=dataDictionary, schema = schema)
#df.printSchema()
#df.limit(5).show(trancate=False)
#df.show(truncate=False)
#display(df.head(5))

#df.write.mode("overwrite").parquet(input_path)
#df.write.mode('overwrite').format("parquet") \
#    .sortBy("load_offset") \
#    .partitionBy("rec_eff_date",) \
#    .bucketBy(3,"category") \
#    .option("path", output_path) \
#    .saveAsTable("feature_transactions_raw")

# check the number of partitions
#num_partitions = df.rdd.getNumPartitions()
#print(num_partitions)

# check the partitions data
#for i, part in enumerate(df.rdd.glom().collect()):
#    print({i: part})
    
#df.printSchema()
#df.limit(5).show(trancate=False)
#df.show(truncate=False)
#display(df.head(5))

# COMMAND ----------

#np.random.random_sample((5, )).tolist()
#np.random.random_sample((5, )).tolist()
map_size = random_map2_size[0]
np.random.random_sample((map_size, )).tolist()

# COMMAND ----------

display(dbutils.fs.ls("/FileStore/datasets/citi/scd_transactions"))
#display(dbutils.fs.ls("/FileStore/datasets/citi/scd_transactions/category=CAT_0"))

# COMMAND ----------

spark.sql("ANALYZE TABLE scd_transactions COMPUTE STATISTICS")

# Task 0: Reading data source from parquet table
transactions = spark.sql("select * from scd_transactions")

#output_path="/FileStore/datasets/citi/tmp_feat_transactions_raw"
#raw_df=spark.read.parquet(output_path)
print("Dataframe shape is {}x{}".format(transactions.count(),len(transactions.columns)))
transactions.printSchema()
#transactions.show(truncate=False)
display(transactions)

# COMMAND ----------



# COMMAND ----------

### initial store table creations
from pyspark.sql.types import StructField, StructType, StringType, IntegerType,FloatType, BooleanType, MapType, DateType, TimestampType, ArrayType
categories = transactions.select("category").rdd.flatMap(lambda x: x).distinct().collect()

output_path="/FileStore/datasets/citi/"
table_name = "scd_store"

dataDictionary2 = {}

schema2 = StructType([
  StructField('cust_id', IntegerType(), True), 
  StructField('rec_eff_date', DateType(), True),
  StructField('load_offset', IntegerType(), True),
])

for category in categories:
    cat_store = table_name + "_{}".format(category)
    category = "{}".format(category)
    df = spark.createDataFrame(data=dataDictionary, schema = schema2)
    df.write.mode('overwrite').format("parquet") \
          .partitionBy("rec_eff_date","load_offset") \
          .option("path", output_path + cat_store) \
          .saveAsTable(cat_store)
    
display(dbutils.fs.ls("/FileStore/datasets/citi/"))

# COMMAND ----------

spark.sql("select * from " + cat_store).show()

# COMMAND ----------

###############################################################
# Step 0: Imports and Spark session creation
###############################################################
from pyspark.sql import SparkSession
from pyspark.sql.functions import map_values,map_concat,col,map_keys
spark = SparkSession.builder.master("yarn").appName("PySpark CAR Poc").getOrCreate()


#functions
def get_col_names(SetA):
    ls = []
    for (col_name, col_type) in SetA:
      name = "{}".format(col_name)
      ls.append(name)
    return ls
  
def getRecommended_DDL_Cmd(dbName,tblName,SetA):
    ls = ""
    for (col_name, col_type) in SetA:
      column = "{} {}".format(col_name, col_type)
      ls = ls + ',' + str(column)
    cmd = "ALTER TABLE " + str(dbName + "." + tblName) + " ADD COLUMNS(" + "{}".format(ls.lstrip(",")) + ")" 
    return cmd

# COMMAND ----------


#publishing code
database = "default"
trans_table = "scd_transactions"
store_table = "scd_store"
trans_max_offsets = {}
store_max_offsets = {}
rec_date = "2021-01-01"

spark.sql("ANALYZE TABLE scd_transactions COMPUTE STATISTICS")
# Task 0: Reading data source from parquet table
#transactions = spark.sql("select * from scd_transactions")
query = "select * from " +  str(trans_table) 
#+ " where rec_eff_date=" + rec_date
transactions = spark.sql(query)

#store = transactions.select("cust_id","load_offset","rec_eff_date")
exclude_col_set = [('feature_map1', 'map<string,int>'), ('feature_map2', 'map<string,float>'), ('feature_map3', 'map<string,boolean>'), ('category', 'string')]
map_columns = ['feature_map1','feature_map2','feature_map3']
categories = transactions.select("category").rdd.flatMap(lambda x: x).distinct().collect()
categories =['CAT_4']

for category in categories:
  cat_store = store_table + "_{}".format(category)
  category = "{}".format(category)
  print(cat_store)
  print("===============================================================================================")
  print("====================== For Category Store : {}".format(cat_store) + "==================================")
  print("===============================================================================================")
  #store = spark.sql("select * from " + cat_store + " WHERE rec_eff_date =" + rec_date)
  #store = transactions.filter((transactions.category == category) & (transactions.rec_eff_date == rec_date)).select("cust_id","load_offset","rec_eff_date")
  ## get store table created 
  store = spark.sql("select * from " + cat_store)
  store.filter(store.rec_eff_date == rec_date)
  
  # Step 1: find incremental data to publish
  cat_transactions = transactions.filter((transactions.category == category)& (transactions.rec_eff_date == rec_date))
  
  if cat_transactions.count() > 0:
    # Step 2: calculate max load_offsets
    store_max_offset = store.filter(store.rec_eff_date == rec_date).agg({"load_offset": "max"}).first()[0]
    store_max_offsets[category] = 0 if (store_max_offset == None) else store_max_offset
    print ("Store table Max Offset for category {} and rec_eff_date: {} are {}".format(category, rec_date, store_max_offset))
    trans_max_offset = cat_transactions.agg({"load_offset": "max"}).first()[0]
    trans_max_offsets[category] = 0 if (trans_max_offset == None) else trans_max_offset
    print ("Transactions table Max Offset for category {} and rec_eff_date: {} are {}".format(category, rec_date, trans_max_offset))
    
    # Step 3: expand the map features of the incremental dataframe
    exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")]
    map_columns = ['feature_map1','feature_map2','feature_map3']
    for mapcol in map_columns:
      map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
      exprs = exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]
    cat_trans = cat_transactions.select(* exprs)
    
    # Step 4: check publish-ability
    #Full list of columns from category store
    sschema = store.dtypes
    print("Full list of columns from category store:")
    print(get_col_names(sschema))
    print("--------------------------------------------------")

    # full list of columns types from delta table
    tschema = cat_trans.dtypes
    print("Full list of columns from delta table:")
    #print(tschema)
    print(get_col_names(tschema))
    print("--------------------------------------------------")

    # full list of columns from delta table after removing map columns
    tschema_no_maps = set(tschema) - set(exclude_col_set)
    print("Full list of columns from delta table after removing map columns:")
    print(get_col_names(tschema_no_maps))
    print("--------------------------------------------------")

    #publishable columns or common columns
    pub_columns = set(tschema_no_maps).intersection(set(sschema))
    #or pub_columns = set(tschema) & set(sschema)
    print("publishable or common columns available in both delta and store tables:")
    print(get_col_names(pub_columns))
    pub_col_list = get_col_names(pub_columns)
    print(pub_col_list)
    print("--------------------------------------------------")

    #Non-publishable or Hidden: Columns that are found in delta but are not available in Store
    delta_diff = set(tschema_no_maps) - set(sschema)
    print("Non-publishable or hidden columns found in delta table but not in the store:")
    print(get_col_names(delta_diff))
    print("--------------------------------------------------")

    #Non-publishable or Hidden: Columns that are found in Store but not in delta
    store_diff = set(sschema) - set(tschema_no_maps)
    print("non-publishable or hidden columns found in store but not in delta:")
    print(get_col_names(store_diff))
    print("--------------------------------------------------")

    #Final publishable delta table
    print("final publishable dataset found in delta:")
    final_delta = cat_trans.select(pub_col_list)
    final_delta.show()
    print("--------------------------------------------------")
    
    #Merged data ready to publish to store
    print("final publishable data - delta merged with store:")
    publishable = final_delta
    publishable.unionByName(store)
    publishable.show()
    #publishable.write.mode("append").partitionBy("rec_eff_date","load_offset").insertInto(cat_store)
    print("--------------------------------------------------")
    
 
    #Recommended DDL command for the store table
    print("--------------------------------------------------")
    print("Recommended DDL command to update schema changes for category store table {}".format(cat_store))
    print("--------------------------------------------------")
    dbName = database
    tblName = cat_store
    print(getRecommended_DDL_Cmd(dbName,tblName,delta_diff))
    print("--------------------------------------------------")
    
    #Recommended DML command for the store table
    print("--------------------------------------------------")
    print("Recommended DML command to merge transactions to the category store table {}".format(cat_store))
    print("--------------------------------------------------")
    dbName = database
    tblName = cat_store
    #print(getRecommended_MERGE_Cmd(dbName,srcTblName,category, cat_transactions))
    print("--------------------------------------------------")
    
  else:
    print("No incremental data found to publish for store " + cat_store)
 
  
#print ("Transactions table Max Offsets for rec_eff_date: {} are {}".format(rec_date, trans_max_offsets))
#print ("Store table Max Offsets for rec_eff_date: {} are {}".format(rec_date, store_max_offsets))

# COMMAND ----------

# Step 3: expand the map features of the delta dataframe

database = "default"
trans_table = "scd_transactions"
store_table = "scd_store"

category = 'CAT_4'
cat_store = store_table + "_{}".format(category)
cat_store_db_tbl = database + "." + cat_store


left_tbl_exprs =[]
right_tbl_exprs =[]
action_exprs = []
values_exprs = []
join_exprs =[]
query_exprs =[]

tbl_alias = ['fT','fs'+(category)]

join_type ='FULL OUTER JOIN'

on_join_keys = ['cust_id','rec_eff_date']
on_join_condn = ["{}.{}={}.{}".format(tbl_alias[0], jk,tbl_alias[1], jk) for jk in on_join_keys]
on_join_condn = [' AND '.join(map(str, on_join_condn))]

src_filter_condn = ['category='+"'{}'".format(category), 'rec_eff_date='+"'{}'".format(rec_date), 'load_offset>'+ str(store_max_offsets[category])]
src_filter_condn = [' AND '.join(map(str, src_filter_condn))]

map_columns = ['feature_map1','feature_map2','feature_map3']
store_partition_keys = ['rec_eff_date='+str(rec_date), 'load_offset='+str(trans_max_offsets[category])]
store_partition_cols = ['rec_eff_date', 'load_offset']
store_nonmap_cols = ['cust_id','rec_eff_date', 'load_offset']
nonmap_cols = set(store_nonmap_cols) - set(store_partition_cols)

#col_exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")]
left_tbl_exprs = ["SELECT * FROM " + trans_table + " WHERE "] + src_filter_condn
right_tbl_exprs = ["SELECT * FROM " + cat_store_db_tbl + " WHERE load_offset=" + str(store_max_offsets[category]) + " OR " + "load_offset" + " IS NULL" ] 
action_exprs = ["INSERT INTO " + cat_store_db_tbl + " PARTITION("] + [",". join(store_partition_keys) + ")"]
join_exprs = ["FROM " + tbl_alias[0] + " {} ".format(join_type) + tbl_alias[1] + " ON "] + on_join_condn

#for non_feature columns excluding maps
values_exprs = values_exprs + [("COALESCE(" + tbl_alias[0] + ".{},".format(k) + tbl_alias[1] + ".{}) AS {}".format(k,k)) for k in nonmap_cols]
values_exprs = ["SELECT " + (','.join(map(str, values_exprs))).lstrip(",")]

#for feature cols in maps
for mapcol in map_columns:
    map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
    values_exprs = values_exprs + [(",COALESCE(" + tbl_alias[0] + "." + mapcol + "['{}'],".format(k) + tbl_alias[1] + ".{}) AS {}".format(k,k)) for k in map_features]
    #col_exprs = col_exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]
    
query_exprs = ["WITH " + tbl_alias[0] + " AS("] + left_tbl_exprs + ["),"] + [tbl_alias[1] + " AS("] + right_tbl_exprs + [")"] + action_exprs + values_exprs + join_exprs
query = '\n'.join(map(str, query_exprs))
print(query)
#spark.sql(query)
                  
# Expanded dataframe for selected category             
#cat_trans = cat_transactions.select(* col_exprs)

# COMMAND ----------

dml_dict = {
  dname:""
  src_tblName:""
  tgt_tblName:""
}

def getRecommended_DML_Cmd(dbName,src_tblName,tgt_tblName,SetA):
    database = "default"
    trans_table = "scd_transactions"
    store_table = "scd_store"

    category = 'CAT_4'
    cat_store = store_table + "_{}".format(category)
    cat_store_db_tbl = database + "." + cat_store


    left_tbl_exprs =[]
    right_tbl_exprs =[]
    action_exprs = []
    values_exprs = []
    join_exprs =[]
    query_exprs =[]

    tbl_alias = ['fT','fs'+(category)]

    join_type ='FULL OUTER JOIN'

    on_join_keys = ['cust_id','rec_eff_date']
    on_join_condn = ["{}.{}={}.{}".format(tbl_alias[0], jk,tbl_alias[1], jk) for jk in on_join_keys]
    on_join_condn = [' AND '.join(map(str, on_join_condn))]

    src_filter_condn = ['category='+"'{}'".format(category), 'rec_eff_date='+"'{}'".format(rec_date), 'load_offset>'+ str(store_max_offsets[category])]
    src_filter_condn = [' AND '.join(map(str, src_filter_condn))]

    map_columns = ['feature_map1','feature_map2','feature_map3']
    store_partition_keys = ['rec_eff_date='+str(rec_date), 'load_offset='+str(trans_max_offsets[category])]
    store_partition_cols = ['rec_eff_date', 'load_offset']
    store_nonmap_cols = ['cust_id','rec_eff_date', 'load_offset']
    nonmap_cols = set(store_nonmap_cols) - set(store_partition_cols)

    #col_exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")]
    left_tbl_exprs = ["SELECT * FROM " + trans_table + " WHERE "] + src_filter_condn
    right_tbl_exprs = ["SELECT * FROM " + cat_store_db_tbl + " WHERE load_offset=" + str(store_max_offsets[category]) + " OR " + "load_offset" + " IS NULL" ] 
    action_exprs = ["INSERT INTO " + cat_store_db_tbl + " PARTITION("] + [",". join(store_partition_keys) + ")"]
    join_exprs = ["FROM " + tbl_alias[0] + " {} ".format(join_type) + tbl_alias[1] + " ON "] + on_join_condn

    #for non_feature columns excluding maps
    values_exprs = values_exprs + [("COALESCE(" + tbl_alias[0] + ".{},".format(k) + tbl_alias[1] + ".{}) AS {}".format(k,k)) for k in nonmap_cols]
    values_exprs = ["SELECT " + (','.join(map(str, values_exprs))).lstrip(",")]

    #for feature cols in maps
    for mapcol in map_columns:
        map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
        values_exprs = values_exprs + [(",COALESCE(" + tbl_alias[0] + "." + mapcol + "['{}'],".format(k) + tbl_alias[1] + ".{}) AS {}".format(k,k)) for k in map_features]
        #col_exprs = col_exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]

    query_exprs = ["WITH " + tbl_alias[0] + " AS("] + left_tbl_exprs + ["),"] + [tbl_alias[1] + " AS("] + right_tbl_exprs + [")"] + action_exprs + values_exprs + join_exprs
    query = '\n'.join(map(str, query_exprs))
    #print(query)  
  
    return query

# COMMAND ----------



# COMMAND ----------

qry_exprs
qry = 
query = '\n'.join(map(str, qry_exprs))
print query

# COMMAND ----------

from typing import List, Union
from datetime import datetime
from pyspark.sql import DataFrame
from pyspark.sql.functions import col, to_timestamp, lit, hash, array, explode, md5, concat_ws

SCD_COLS = ["rec_eff_dt", "row_opern"]

def rename_columns(df: DataFrame, alias: str, keys_list: List, add_suffix: int = 1) -> DataFrame:
    """ Rename columns to denote if they belong to source or target

    Args:
        df (DataFrame): dataframe to perform renaming on
        alias (str): alias to add as suffix
        keys_list (List): list of keys which are used for joins
        add_suffix (int, optional): 0 - remove suffix, 1- add suffix. Defaults to 1.

    Returns:
        DataFrame: dataframe with performed adding/removing of suffix
    """
    if add_suffix == 1:
        for column in set(df.columns) - set(SCD_COLS) - set(keys_list):
            df = df.withColumnRenamed(column, column + "_" + alias)
    else:
        for column in [cols for cols in df.columns if f"_{alias}" in cols]:
            df = df.withColumnRenamed(column, column.replace("_" + alias, ""))
    return df


# COMMAND ----------

fti = ""

history_hash = rename_columns(get_hash_column(history, keys_list, ignored_columns), alias="history", keys_list=keys_list)

# COMMAND ----------

  exprs = [col("cust_id")] 
  partitions = [col("rec_eff_date")] + [col("load_offset")]
    map_columns = ['feature_map1','feature_map2','feature_map3']
    for mapcol in map_columns:
      map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
      exprs = exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]
    cat_trans = cat_transactions.select(* exprs)

# COMMAND ----------

def getRecommended_INSERT_Cmd(dbName,tblName,SetA):
    ls = ""
    for (col_name, col_type) in SetA:
      column = "{} {}".format(col_name, col_type)
      ls = ls + ',' + str(column)
    #cmd = "ALTER TABLE " + str(dbName + "." + tblName) + " ADD COLUMNS (" + "{}".format(ls.lstrip(",")) + ")" 
    cmd = "INSERT INTO TABLE " + str(dbName + "." + tblName) + " VALUES (" + <rows list goes here> + ")"
    
    return cmd
  
  
  #'Dikshant',1,'95'),('Akshat', 2 , '96'),('Dhruv',3,'90'

# COMMAND ----------

#Recommended MERGE SQL command for the store table
#print("Recommended MERGE SQL command for the store table:")
dbName = database
tblName = cat_store
print(getRecommended_MERGE_Cmd(dbName,tblName,))
print("--------------------------------------------------")

# COMMAND ----------

spark.sql("""
    insert overwrite table `{schema}`.`{table}`
    partition (partCol1, partCol2)
      select col1       
           , col2       
           , col3       
           , col4   
           , partCol1
           , partCol2
    from {temp_table}
""".format(schema=schema, table=table, temp_table=temp_table)

# COMMAND ----------

cmd = "INSERT INTO TABLE " + str(dbName + "." + tblName) + " VALUES (" + <list goes here> + ")"

INSERT INTO TABLE tablename [PARTITION (partcol1[=val1], partcol2[=val2] ...)] VALUES values_row [, values_row ...]

# COMMAND ----------

exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")]
map_columns = ['feature_map1','feature_map2','feature_map3']
for mapcol in map_columns:
  map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
  exprs = exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]
cat_final_df = cat_transactions.select(* exprs)
                            
#exprs = [col("cust_id")] + [col("rec_eff_date")] + [col("load_offset")] + [col("featuremap").getItem(k).alias(k) for k in features]
# final_df_cat = df_cat.select(* exprs)

# COMMAND ----------

cat_final_df.dtypes

# COMMAND ----------

# Max Load offsets across categories for transactions data
from pyspark.sql.window import Window
from pyspark.sql.functions import col,avg,sum,min,max,row_number 

windowSpecAgg  = Window.partitionBy("category","rec_eff_date").orderBy(col("load_offset").desc())
trans_max_offsets = transactions.withColumn("row",row_number().over(windowSpec))\
.withColumn("max_offset", max(col("load_offset")).over(windowSpecAgg)) \
.where(col("row")==1).select("category","max_offset")
trans_max_offsets.show()

windowSpecAgg  = Window.partitionBy("rec_eff_date").orderBy(col("load_offset").desc())
store_max_offsets = store.withColumn("row",row_number().over(windowSpec)) \
.withColumn("max_offset", max(col("load_offset")).over(windowSpecAgg)) \
.where(col("row")==1).select("max_offset")
store_max_offsets.show()

#store_max_offset = spark.sql("select MAX(load_offset) from " + str(store)).first()[0]

#transactions.withColumn("row",row_number.over(windowDept)).where("row" == 1).drop("row").show()

# COMMAND ----------

#transactions.groupby("category").max('load_offset').first().asDict()['max(load_offset)']
#store.groupby("rec_eff_date").max('load_offset').first().asDict()['max(load_offset)']

# Method 1: Use describe()
#int(store.describe("load_offset").filter("summary = 'max'").select("load_offset").first().asDict()['load_offset'])

#Method 2: using sql
#transactions.registerTempTable("df_table")
#spark.sql("SELECT MAX("load_offset") as maxval FROM df_table").first().asDict()['maxval']


# Method 4: Convert to RDD
tmax = transactions.select("load_offset").rdd.max()[0]
smax = store.select("load_offset").rdd.max()[0]

ttmax = transactions.agg({"load_offset": "max"}).first()[0]

tttmax = transactions.filter((transactions.category == "CAT_4") & (transactions.rec_eff_date == "2021-01-01")).agg({"load_offset": "max"}).first()[0]

print(tmax,smax,ttmax, tttmax)

# COMMAND ----------

# Max Load offsets across categories for transactions
from pyspark.sql.window import Window
from pyspark.sql.functions import col,avg,sum,min,max,row_number 

def get_max_loadoffsets(dataframe,partitionbycols,target_col):
  partitionbycols = partitionbycols
  windowSpecAgg  = Window.partitionBy(partitionbycols).orderBy(col(target_col).desc())
  result_df =  dataframe.withColumn("row",row_number().over(windowSpec)) \
  .withColumn("max", max(col(target_col)).over(windowSpecAgg)) \
  .where(col("row")==1).select(partitionbycols,"max") 
  return result_df
trans_partitionby_cols = ['category','rec_eff_date']
store_partitionby_cols = ['rec_eff_date']
max_of_colname = 'load_offset'
result = get_max_loadoffsets(transactions,trans_partitionby_cols,max_of_colname)
result.show()



# Method 1: Use describe()
float(df.describe("A").filter("summary = 'max'").select("A").first().asDict()['A'])

# Method 2: Use SQL
df.registerTempTable("df_table")
spark.sql("SELECT MAX(A) as maxval FROM df_table").first().asDict()['maxval']

# Method 3: Use groupby()
df.groupby().max('A').first().asDict()['max(A)']

# Method 4: Convert to RDD
df.select("A").rdd.max()[0]

# COMMAND ----------

store_max_offset = spark.sql("select MAX(load_offset) from " + str(trans_table)).first()[0]


df.createOrReplaceTempView("EMP")
spark.sql("select Category, ReLoad_Offset from "+
     " (select *, row_number() OVER (PARTITION BY category ORDER BY load_offset DESC) as rn " +
     " FROM default.scd_transactions) tmp where rn = 1").show()
print(store_max_offset)

# COMMAND ----------


#publishing code
store = transactions.select("cust_id","load_offset","rec_eff_date")
exclude_col_set = [('feature_map1', 'map<string,int>'), ('feature_map2', 'map<string,float>'), ('feature_map3', 'map<string,boolean>'), ('category', 'string')]

#Full list of columns from category store
sschema = store.dtypes
print("Full list of columns from category store:")
print(get_col_names(sschema))
print("--------------------------------------------------")

# full list of columns types from delta table
tschema = transactions.dtypes
print("Full list of columns with types from delta table:")
print(tschema)
print(get_col_names(tschema))
print("--------------------------------------------------")

# full list of columns from delta table after removing map columns
tschema_no_maps = set(tschema) - set(exclude_col_set)
print("Full list of columns from delta table after removing map columns:")
print(get_col_names(tschema_no_maps))
print("--------------------------------------------------")

#publishable columns or common columns
pub_columns = set(tschema_no_maps).intersection(set(sschema))
#or pub_columns = set(tschema) & set(sschema)
print("publishable columns or common columns found in both delta and store tables:")
print(get_col_names(pub_columns))
pub_col_list = get_col_names(pub_columns)
print(pub_col_list)
print("--------------------------------------------------")

#non-publishable columns from both tables
diff = set(tschema_no_maps) ^ set(sschema)
print("non-publishable columns from both delta and store tables:")
print(get_col_names(diff))
print("--------------------------------------------------")

#Non-publishable or Hidden: Columns that are found in delta but are not available in Store
delta_diff = set(tschema_no_maps) - set(sschema)
print("Non-publishable or hidden columns found in delta table but not in the store:")
print(get_col_names(delta_diff))
print("--------------------------------------------------")

#Non-publishable: Columns that are found in Store but not in delta
store_diff = set(sschema) - set(tschema_no_maps)
print("non-publishable columns: found in store but not in delta:")
print(get_col_names(store_diff))
print("--------------------------------------------------")

#finally publishable delta table
print("final publishable dataset found in delta:")
pd_table = transactions.select(pub_col_list)
pd_table.show()

print("--------------------------------------------------")

#recommended ALTER command for the store table
print("Recommended ALTER command for the store table:")
dbName ="default"
tblName = "store"
print(getRecommended_Cmd(dbName,tblName, delta_diff))
print("--------------------------------------------------")

# COMMAND ----------

df.properties.getItem("hair")

# COMMAND ----------

np.random.randint(min_map_size,max_map_size,1)[0]

# COMMAND ----------

dict_map1
dict_map2
dict_map3

dataDictionary

# COMMAND ----------

print(dataObj)
#temp = spark.createdataframe(data=dataObj, schema = schema)
dd = []
dd.append(dataObj)
dd.append(dataObj)
print(dd)
temp = spark.createDataFrame(data=dd, schema = schema)
display(temp)

# COMMAND ----------

display(transactions)

# COMMAND ----------



# COMMAND ----------



# COMMAND ----------



# COMMAND ----------

##spark_singleton.py
from pyspark.sql import SparkSession
import os

class SparkSingleton:
    """
    Creates a singleton class which returns one spark instance, if it is not created,
    or else returns the previous singleton object.
    """

    @classmethod
    def get_instance(cls):
        """Create a Spark instance for FSManagerUtil.
        :return: A Spark instance
        """
        return SparkSession.builder.getOrCreate()

# COMMAND ----------

class CrawlerSingleton(object):
    def __new__(cls):
        """ creates a singleton object, if it is not created,
        or else returns the previous singleton object"""
        if not hasattr(cls, 'instance'):
            cls.instance = super(CrawlerSingleton, cls).__new__(cls)
        return cls.instance

# COMMAND ----------

##manager.py
import os

class FeatureStoreManager():
    '''This class is created to invoke operations and manage the Featurestore life cycle.'''
    def __init__(self, name="Noor", age=42):
      
        self._name = name
        self._age = age
    
    def get_data(self):
      print(f'{self._name}+{self._age}')

def main():
  pass

if __name__ == "__main__":
    main()

# COMMAND ----------

##helpers.py
from framework.spark_singleton import SparkSingleton
from pyspark.sql.functions import col

import logging

logger = logging.getLogger(__name__)
spark = SparkSingleton.get_instance()

class Helpers:
    def __init__(self):
        pass
    
    def function1():
        pass
      
