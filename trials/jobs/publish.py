from dependencies import logger
from pyspark.sql.functions import map_values, map_concat, col, map_keys
from pyspark.sql.types import *

#functions
def get_col_names(SetA):
    ls = []
    for (col_name, col_type) in SetA:
      name = "{}".format(col_name)
      ls.append(name)
    return ls

def check_publishability(config, cat_trans_df, store_df):
    """ check publish-ability """
    #Full list of columns from category store
    sschema = store_df.dtypes
    logger.info("Full list of columns from category store:")
    logger.info(get_col_names(sschema))
    logger.info("--------------------------------------------------")

    # full list of columns types from delta table
    tschema = cat_trans_df.dtypes
    logger.info("Full list of columns from delta table:")
    #logger.info(tschema)
    logger.info(get_col_names(tschema))
    logger.info("--------------------------------------------------")

    # full list of columns from delta table after removing map columns
    tschema_no_maps = set(tschema) - config.get("exclude_col_set")
    logger.info("Full list of columns from delta table after removing map columns:")
    logger.info(get_col_names(tschema_no_maps))
    logger.info("--------------------------------------------------")

    #publishable columns or common columns
    pub_columns = set(tschema_no_maps).intersection(set(sschema))
    #or pub_columns = set(tschema) & set(sschema)
    logger.info("publishable or common columns available in both delta and store tables:")
    logger.info(get_col_names(pub_columns))
    pub_col_list = get_col_names(pub_columns)
    logger.info(pub_col_list)
    logger.info("--------------------------------------------------")

    #Non-publishable or Hidden: Columns that are found in delta but are not available in Store
    delta_diff = set(tschema_no_maps) - set(sschema)
    logger.info("Non-publishable or hidden columns found in delta table but not in the store:")
    non_publishable_features = get_col_names(delta_diff)
    logger.info(non_publishable_features)
    logger.info("--------------------------------------------------")

    #Non-publishable or Hidden: Columns that are found in Store but not in delta
    store_diff = set(sschema) - set(tschema_no_maps)
    logger.info("non-publishable or hidden columns found in store but not in delta:")
    logger.info(get_col_names(store_diff))
    logger.info("--------------------------------------------------")
    
    #Final publishable delta table
    logger.info("final publishable dataset found in delta:")
    final_delta = cat_trans_df.select(pub_col_list)
    final_delta.show()
    logger.info("--------------------------------------------------")
    
    if (len(non_publishable_features)>0):
      return False, delta_diff
    else:
      return True, final_delta

def get_recommended_DDL_query(config, SetA):
    """ function to generate the DDL command """
    ls = ""
    for (col_name, col_type) in SetA:
      column = "{} {}".format(col_name, col_type)
      ls = ls + ',' + str(column)
      tbl_name = config.get("store_cat_table") # make sure category text is replaced with actual category
    cmd = "ALTER TABLE " + tbl_name + " ADD COLUMNS(" + "{}".format(ls.lstrip(",")) + ")" 
    return cmd
  
def get_recommended_DML_query(config, SetA):
    """ function to generate the DML query """
    cmd =" DML Not yet generated "
    return cmd  

def run_job(spark, config):
    """ function to run the spark job """
    spark.sql("ANALYZE TABLE " + config.get("trans_table") + "COMPUTE STATISTICS")
    query = "select * from " +  config.get("trans_table") + " where rec_eff_dt=" + config.get("rec_eff_dt")
    
    # get transactions to publish 
    transactions = spark.sql(query)
    
    # get category list of publishable transactions
    categories = transactions.select("category").rdd.flatMap(lambda x: x).distinct().collect()
    rec_date = config.get("rec_eff_dt")
    
    for category in categories:
      cat_store = config.get("store_table") + "_{}".format(category)
      category = "{}".format(category)
      logger.info(cat_store)
      logger.info("=========================================================================")
      logger.info(" For Category Store : {}".format(cat_store) )
      logger.info("=========================================================================")
      store = spark.sql("select * from " + cat_store + " where rec_eff_dt=" + rec_date)
      #store = store.filter(store.rec_eff_date == rec_date)
      
      # Step 1: find incremental data to publish
      cat_transactions = transactions.filter((transactions.category == category)& (transactions.rec_eff_date == rec_date))
      
      if cat_transactions.count() > 0:
        # Step 2: calculate max load_offsets
        store_max_offset = store.agg({"load_offset": "max"}).first()[0]
        config.store_max_offsets[category] = 0 if (store_max_offset == None) else store_max_offset
        logger.info("Store table Max Offset for category {} and rec_eff_date: {} are {}".format(category, rec_date, store_max_offset))
        trans_max_offset = cat_transactions.agg({"load_offset": "max"}).first()[0]
        config.trans_max_offsets[category] = 0 if (trans_max_offset == None) else trans_max_offset
        logger.info("Transactions table Max Offset for category {} and rec_eff_date: {} are {}".format(category, rec_date, trans_max_offset))
        
        # Step 3: expand the map features of the incremental dataframe
        exprs = [col("cust_id")] + [col("rec_eff_dt")] + [col("load_offset") + [col("rec_prcs_dt_tm")]]
        map_columns = config.get("map_columns")
        for mapcol in map_columns:
          map_features = cat_transactions.select(map_keys(mapcol)).distinct().rdd.flatMap(lambda x: x[0]).collect()
          exprs = exprs + [col(mapcol).getItem(k).alias(k) for k in map_features]
        cat_trans = cat_transactions.select(* exprs)
        
        # Step 4: check publish-ability
        proceed, result_df = check_publishability(config, cat_trans, store)
        if proceed:
          # publishable
          query = get_recommended_DML_query(config,)
          #Recommended DML command for the store table
          logger.info("--------------------------------------------------")
          logger.info("Recommended DML command to update store table {}".format(cat_store))
          logger.info("--------------------------------------------------")
          logger.info(query)
          logger.info("--------------------------------------------------")
          #spark.sql(query)
        else:
          # Non-publishable
          query = get_recommended_DDL_query(config, result_df)                
          #Recommended DDL command for the store table
          logger.info("--------------------------------------------------")
          logger.info("Recommended DDL command for store table {}".format(cat_store))
          logger.info("--------------------------------------------------")
          logger.info(query)
          logger.info("--------------------------------------------------")
      else:
        logger.info("No incremental data found to publish for store " + cat_store)  
    