"""
logging
~~~~~~~
This module contains logging code for PySpark using.
"""

import os
import sys
import logging
from datetime import datetime

# Logging configuration
formatter = logging.Formatter('[%(asctime)s] %(levelname)s @ line % (lineno)d: % (message)s')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)

logger = logging.getLogger("FeatureStoreJobManager")
logger.setLevel(logging.INFO)
logger.addhandler(handler)

# current time variable to be used for logging purpose
dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")