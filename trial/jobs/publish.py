from pyspark.sql.functions import map_values, map_concat, col, map_keys
from pyspark.sql.types import *

#functions
def get_col_names(SetA):
    ls = []
    for (col_name, col_type) in SetA:
      name = "{}".format(col_name)
      ls.append(name)
    return ls
  
def get_recommended_DDL(config, SetA):
    ls = ""
    for (col_name, col_type) in SetA:
      column = "{} {}".format(col_name, col_type)
      ls = ls + ',' + str(column)
      tbl_name = config.get("dbName") + "." + config.get("tblName")
    cmd = "ALTER TABLE " + tbl_name + " ADD COLUMNS(" + "{}".format(ls.lstrip(",")) + ")" 
    return cmd




def run_job(spark, config):